<?php

namespace CodeigniterDatabase;

use Evolution\CodeIgniterDB as CI;

/**
 * CodeigniterDatabase
 */
class CodeigniterDatabase
{
    public $db;

    private $map = array();

    private $table;

    public function __construct($config, $map = array())
    {
        $this->db = &CI\DB($config);
        $this->map = $map;
        $this->join = "";
    }

    public function __call($name, $arguments = "*")
    {
        if(method_exists($this->db,$name)) {
            $callback_function = array($this->db, $name);
            call_user_func_array($callback_function, $arguments);
        }
        else if(isset($this->map[$name])) {
            $map = $this->map[$name];
            $this->db->join($map[1],$map[2],"LEFT");
            $this->db->from($map[0]);
            $this->table = $map[0];
            $this->join = " LEFT JOIN " . $map[1] . " on (" . $map[2] . ")";
        } else {
            $this->table = $name;
            $this->db->from($name);
        }
        return $this;
    }

    public function setMap($data)
    {
        $this->map = array_merge($data,$this->map);
    }

    public function getRow()
    {
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRows()
    {
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete()
    {
        $this->db->delete($this->table);
        return $this;
    }

    public function update($data)
    {
        $this->db->update($this->table, $data);
        return $this;
    }

    public function sql_row($sql)
    {
        $q = $this->db->query($sql);
        return $q->row_array();
    }

    public function sql_rows($sql)
    {
        $q = $this->db->query($sql);
        return $q->result_array();
    }

    public function datatable_json($config)
    {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $aColumns = $config['aColumns'];

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = $config['sIndexColumn'];

        /* DB table to use */
        $sTable = isset($config['sTable']) ? $config['sTable'] : $this->table;

        /* Query condition  */
        $sCondition = isset($config['sCondition']) ? $config['sCondition'] : '';

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['start']) && $_GET['length'] != '-1') {
            $sLimit = "LIMIT " . intval($_GET['start']) . ", " .
            intval($_GET['length']);
        }

        /*
         * Ordering
         */
        /*
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($_GET['iSortCol_' . $i])] . "` " .
                        ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }*/

        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($_GET['order'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['order']); $i++) {
                $sOrder .= "`" . $aColumns[intval($_GET['order'][$i]['column'])] . "` " .
                    ($_GET['order'][$i]['dir'] === 'asc' ? 'asc' : 'desc') . ", ";
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if (isset($_GET['search']['value']) && $_GET['search']['value'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '" . $_GET['search']['value'] . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
            }
        }

        if ($sWhere == "" && $sCondition != "") {
            $sWhere .= 'where (' . $sCondition . ') ';
        } else if ($sCondition != "") {
            $sWhere .= ' AND (' . $sCondition . ') ';
        }

        /*
         * SQL queries
         * Get data to display
         */
        $sTable .= $this->join;
        $sQuery = "
          SELECT SQL_CALC_FOUND_ROWS `" . str_replace(" , ", " ", implode("`, `", $aColumns)) . "`
          FROM   $sTable
          $sWhere
          $sOrder
          $sLimit
        ";

        /* fetching result from database */
        $rResult = $this->db->query($sQuery);
        $sQuery = "
          SELECT COUNT(`" . $sIndexColumn . "`) as 'count'
          FROM $sTable $sWhere
        ";
        $rResultTotal = $this->db->query($sQuery);
        $rResultTotal = $rResultTotal->row_array();
        $iTotal = $rResultTotal["count"];
        $iFilteredTotal = $iTotal;

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval(isset($_GET['sEcho'])?$_GET['sEcho']:""),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array(),
        );

        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            extract($aRow);

            foreach ($config['aColumns_out'] as $aColumnsrow) {
                $col = "";
                foreach ($aColumnsrow as $key => $value) {
                    if ($key == 'text') {
                        $col .= $value;
                    } else if ($key == 'var') {
                        $col .= $aRow[$value];
                    } else if ($key == 'html') {
                        if (preg_match_all('/\{{(.*?)\}}/', $value, $match)) {
                            foreach ($match[1] as $v => $k) {
                                $match[1][$v] = $aRow[$k];
                            }

                            $col .= str_replace($match[0], $match[1], $value);
                        } else {
                            $col .= $value;
                        }

                    } else if ($key == 'eval') {
                        eval("\$col = " . $value . ";");
                    } else {
                        $col .= 'Invalid type!';
                    }

                }
                $row[] = $col;
            }

            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }
}
